# README #

This is for the final project of CSE 401 at the University of Washington. We are building a layer on top of HTML to add abstraction and control flow without needing to use JavaScript. Our main goal is to life simple for people trying to build small web pages without needing to use JavaScript or PHP.

## Documents ##
[Pre-Proposal](https://docs.google.com/document/d/1ObqribPykcxXHWiFiOWAbj3_3vigJwkLzXyi56U8HHM/edit?usp=sharing)

[Proposal](https://docs.google.com/a/uw.edu/document/d/1NKibVCfxrENAbqwZrOsjbPYJIxduZ_dFY1hzWHenkuI/edit?usp=sharing)

[Design Document](https://docs.google.com/document/d/1b29SBmzVrG0D_MBrvZy6grHkteCH1vIwTD4NvhZfRxw/edit?usp=sharing)

[Report](https://docs.google.com/document/d/1DCS8EO-q6tKLJuvR01Erz0nLcS-upHoQNbnGdQYkSoA/edit?usp=sharing)

[Demo](https://drive.google.com/open?id=1GCatGqh8gd1k5GZONrnYMj0ckgE0wfZC)

## Tutorial ##
First, copy the existing [starter.html](https://bitbucket.org/atgeller/html/src/561606adebcc3294a06cfda4360c662e42efbd66/starter.html?at=master) file, and replace `{your document name}` with the name of your htmlpp file on line 4. Now you can use HTML++ code
in your htmlpp file.Make sure starter.html is always in the same folder as your htmlpp file and the `src` folder.

HTML++ is a strict superset of HTML, meaning you can write HTML code around and in HTML++. You can even have an HTML++ webpage that is only HMTL.

HTML++ extends six tags on top of vanilla HTML:

- <define my-element [var1. var2, …]>
	This element allows the user to create template elements composed of other html tags. For example, `<define title name><h1>My name is <print name></h1></define>` would define a new element, `<title>`, which would be used like `<title name=”Ras”>`
+ <foreach element in data>
	The foreach tag allows the user to iterate over items in a collection, like a standard foreach loop in most programming languages. It allows the user to write code once that will be run on each item in the given data. For example, `<foreach movie in movies><li><print movie></li></foreach>` Would print every element in the data object movies.
* <if cond="condition"><elseif cond="condition"><else>
	This element allows the user to add control flow to their HTML. For example:
	```
	<data>{name:“Ras”}</data>
	<if name==”Chris”>...
	<elseif name==”Bill”>...
	<else>...</if>
	```
	We monitor the variables used in the conditions for changes, so that the page will respond dynamically as the data changes.
* <data>{...}</data> or <import src=”...” /> or <import doc=”...” />
	The data tag allows the user to define JSON data to be used in the website. The import tag works the same as the data tag, but pulls the data from a file or web source, instead of from between the data tags. The name is optional, for if the user wants to load the data into a given variable name, otherwise the data will be loaded into variables with the names of their keys in the JSON object.
* <print var>
	This directly prints the given variable into the HTML at the specified location. For example: `<data>{name:”Ras”}</data><h1><print name></h1>` would render in the browser as `<h1>Ras</h1>`

There are a few notes:
* HTML++ does not play nicely with tables (because they are old). But that's ok because floats and lists are newer and better.
* HTML++ requires you to be running a local server in the same directory as the htmlpp file in order to retrieve the JavaScript files required for compilation. I recommend downloading python and using `python -m http.server 8000`. Now you can open `localhost:8000` in a browser and see your website!

To import data from a Google Sheet:
1. Make sure your sheet is publicly viewable and readable
2. In you HTML++ code, use `<import doc="{id}">`, replacing `{id}` with the id of your Google sheet.
	You can get the id parameter from the URL of your sheet. For example: the id of this sheet: https://docs.google.com/spreadsheets/d/1H_2SnEcazm4RJgelI0aBBHF_Sy_VA2EjIitZjOVqZ30/edit is `1H_2SnEcazm4RJgelI0aBBHF_Sy_VA2EjIitZjOVqZ30`
3. Using the data:  
	* The data is grouped by rows  
	* The first value in a row is considered the row header  
	* To iterate through a row, use `<foreach {varname} in {row header}>`. For example, printing a list of names in the `Name` row of our [example sheet](https://docs.google.com/spreadsheets/d/1H_2SnEcazm4RJgelI0aBBHF_Sy_VA2EjIitZjOVqZ30/edit) would be done using `<ul><foreach name in Name><li><print>name</print></li></foreach></ul>`

For an easy example, check out [example1.htmlpp](https://bitbucket.org/atgeller/html/src/561606adebcc3294a06cfda4360c662e42efbd66/example1.htmlpp?at=master&fileviewer=file-view-default). For a medium example, check out [example2.htmlpp](https://bitbucket.org/atgeller/html/src/561606adebcc3294a06cfda4360c662e42efbd66/example2.htmlpp?at=master&fileviewer=file-view-default). For a hard example, check out [example3.htmlpp](https://bitbucket.org/atgeller/html/src/561606adebcc3294a06cfda4360c662e42efbd66/example3.htmlpp?at=master&fileviewer=file-view-default).
	