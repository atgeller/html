var uniqueID = 1;

class ASTNode {
    constructor(type, create = true) {
		this.type = type;
		this.id = uniqueID++;
		if (create) {
			this.span = document.createElement(`span`);
			this.span.id = `${this.type}${this.id}`;
		}
	}

	compile (parent = undefined) {
		if (parent && parent.span && this.span)
			parent.span.appendChild(this.span);
	}

	update (propagate = false, ctx = {}) {
		var result = this.execute(ctx);
		if (propagate && this.span)
			return result;
		else if (this.span)
			document.getElementById(`${this.type}${this.id}`).innerHTML = result;
		else
			return result;
	}

	execute (ctx) {
		alert("Programmer fail: ASTNode.execute was called");
	}
	
	evaluate (expr, ctx) {
		var params = Object.keys(ctx);
        var args = [];
        for(var key in params){
            args.push("ctx." + params[key]);
        }

        params = params.join();
		args = args.join();
        var f = "(function(" + params + "){" +
				"try{ return " + expr + "}catch(e){}})(" + args + ");";
		return f;
	}
}

class SequenceNode extends ASTNode {
	constructor(ctx = undefined, create = true) {
		super("Sequence", create);
		this.nodes = [];
		this.ctx = ctx;
	}

	compile (parent = undefined) {
		var res = "";
		for(var i in this.nodes) {
			var x = this.nodes[i].compile(this);
			if (x) res += x;
		}

		super.compile(parent);
		return res;
	}

	execute (ctx) {
		var context;
		if (this.ctx)
			context = this.ctx;
		else
			context = ctx;

		var res = "";
		for(var i in this.nodes) {
			var x = this.nodes[i].update(this.id !== 1, context);
			if (x) res += x;
		}
		return res;
	}

	push () {
        for(var i in arguments)
            this.nodes.push(arguments[i]);
	}
}

class HTMLNode extends ASTNode {
	constructor(text, create = false) {
		super("HTML", false, create);
		this.text = text;
	}

	compile (parent = undefined) {
		return this.text;
	}

	execute (ctx) {
		return this.text;
	}
}

class PrintNode extends ASTNode {
	constructor(expr, create = true, parent = null) {
		super("Print", create, parent);
		this.expr = expr;
	}

	compile (parent = undefined) {
		super.compile(parent);
		return this.span.outerHTML;
	}

	execute (ctx) {
        // wrap the expression in a function that takes the context keys as params
        // and call it with the context values as arguments
		var f = super.evaluate(this.expr, ctx);
		return String(eval(f));
	}
}

class IfElseNode extends ASTNode {
	constructor(cond, ifBody, elseBody, create = true, parent = null) {
		super("IfElse", create, parent);
		this.cond = cond;
		this.ifBody = ifBody;
		this.elseBody = elseBody;
	}

	compile(parent = undefined) {
		super.compile(parent);
		this.ifBody.compile(this);
		this.elseBody.compile(this);	
		return this.span.outerHTML;
	}

	execute(ctx) {
		var f = super.evaluate(this.cond, ctx);
		if(eval(f)) {
			return this.ifBody.update(true, ctx);
		} else {
			return this.elseBody.update(true, ctx);
		}
	}
}

class ForeachNode extends ASTNode {
	constructor(varName, collection, body, create = true, parent = null) {
		super("Foreach", create, parent);
		this.varName = varName;
		this.collection = collection;
		this.body = body;
	}

	compile(parent = undefined) {
		super.compile(parent);
		return this.span.outerHTML;
	}

	execute(ctx) {
		var result = "";
		for (var elt in ctx[this.collection]) {
			// body.context[varName] = context[collection][elt]
			var extended = ctx;
			extended[this.varName] = extended[this.collection][elt];
			result += this.body.update(true, extended);
		}
		return result;
	}
}

// Work on grabbing from Google doc later
class ImportNode {
	constructor(id) {
		this.id = id;
	}

	compile(parent = undefined) {
		this.parent = parent;
	}

	getValues (data) {
		var result = [];
		for (var row in data) {
			var r = {header: "", data: []};
			for (var i in data[row].values) {
				if (i == 0)
					r.header = data[row].values[i].formattedValue;
				else
					r.data.push(data[row].values[i].formattedValue);
			}
			result.push(r);
		}
		return result;
	}

	update() {
		var request = new XMLHttpRequest();
		var key = "AIzaSyA1HH7eb3updmfgGO5r7vKFgdi768VbBjc";
		//var id = "1H_2SnEcazm4RJgelI0aBBHF_Sy_VA2EjIitZjOVqZ30";
		var source = `https://sheets.googleapis.com/v4/spreadsheets/${this.id}`;
		request.open("GET", `${source}?key=${key}&includeGridData=true`, false/*<false=sync, true=async>*/);
		request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		request.send(null);
		var response = JSON.parse(request.responseText);
		if (response.error) { alert("Error pulling data from id" + this.id); return; }
		var title = response.properties.title;
		var data = response.sheets[0].data[0].rowData;
		var values = this.getValues(data);

		console.log(values);

		for (var i in values) {
			ctx[values[i].header] = values[i].data;
		}

		return "";
	}
}
